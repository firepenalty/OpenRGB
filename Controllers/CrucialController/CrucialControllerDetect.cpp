#include "Detector.h"
#include "CrucialController.h"
#include "LogManager.h"
#include "RGBController.h"
#include "RGBController_Crucial.h"
#include "i2c_smbus.h"
{
    bool pass = false;

    LOG_TRACE("[CRUCIAL] looking for devices at 0x%02X...", address);

    int res = bus->i2c_smbus_write_quick(address, I2C_SMBUS_WRITE);

    if (res >= 0)
    {
        pass = true;

        LOG_DEBUG("[CRUCIAL] Detected an I2C device at address %02X, testing register range", address);

        for (int i = 0xA0; i < 0xB0; i++)
        {
            res = bus->i2c_smbus_read_byte_data(address, i);

            if (res != (i - 0xA0))
            {
                LOG_VERBOSE("[CRUCIAL] Detection failed testing register %02X.  Expected %02X, got %02X.", i, (i - 0xA0), res);

                pass = false;
            }
        }
    }
    if (pass)
    {
        LOG_VERBOSE("[CRUCIAL] Detection successful, address %02X", address);
    }

    return(pass);


        IF_DRAM_SMBUS(busses[bus]->pci_vendor, busses[bus]->pci_device)
        {
            // Remap Crucial RAM modules on 0x27
            LOG_DEBUG("[CRUCIAL DRAM] Remapping Aura SMBus RAM modules on 0x27");

            for (unsigned int slot = 0; slot < 4; slot++)
            {
                int res = busses[bus]->i2c_smbus_write_quick(0x27, I2C_SMBUS_WRITE);

                if (res < 0)
                {
                    LOG_DEBUG("[CRUCIAL DRAM] No device detected at 0x27, aborting remap");
                    break;
                }


                    if(address_list_idx < CRUCIAL_ADDRESS_COUNT)
                    {
                        LOG_DEBUG("[CRUCIAL DRAM] Testing address %02X to see if there is a device there", crucial_addresses[address_list_idx]);
                        res = busses[bus]->i2c_smbus_write_quick(crucial_addresses[address_list_idx], I2C_SMBUS_WRITE);
                    }
                    else

                if(address_list_idx < CRUCIAL_ADDRESS_COUNT)
                {
                    LOG_DEBUG("[CRUCIAL DRAM] Remapping slot %d to address %02X", slot, crucial_addresses[address_list_idx]);
                    CrucialRegisterWrite(busses[bus], 0x27, 0x82EE, slot);
                    CrucialRegisterWrite(busses[bus], 0x27, 0x82EF, (crucial_addresses[address_list_idx] << 1));
                    CrucialRegisterWrite(busses[bus], 0x27, 0x82F0, 0xF0);
                }
            }
        }
}


